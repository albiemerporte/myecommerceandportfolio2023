
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Header from "./header";
import Features from './features';
import Footer from './footer';
import Qoute from './qoute';
import { Globe, Body } from './design';
import Vlogs from './vlogs';
import Project from './project';
import Login from './components/login'
import Tos from './components/tos'

const App = () => {
  return (
    <>
      <Globe />
        <Body>
          <Router>
            {/*header*/}
            <Header />
            <Routes>
              <Route path='/login' element={<Tos />} />
            </Routes>

            <Routes>{/*content*/}
              <Route path='/' element={<Features />} />

              <Route path='/' element={<Features />} />
              
              <Route path='/project' element={<Project />} />

              <Route path='/vlogs' element={<Vlogs />} />

              <Route path='/features' element={<Features />} />

              <Route path='/qoute' element={<Qoute />} />

              <Route path='/login' element={<Qoute />} />
            </Routes>

            {/*footer*/}
              <Footer />
          </Router>
        </Body>
    </>
  );
}

export default App;
