
import { LoginForm } from "../designcss/logincss";
import { Link } from "react-router-dom";
import Tos from "./tos";

const Login = () => {

    return (
        <>
            <div class="container">
                <center> <h1> Student Login Form </h1> </center> 
                <LoginForm action="/login"> 
                    <label>Username : </label> 
                    <input type="text" placeholder="Enter Username" name="username" required />
                    <label>Password : </label> 
                    <input type="password" placeholder="Enter Password" name="password" required />
                    <button className='loginbutton' type="submit">Login</button> 
                    <input type="checkbox" checked="checked" /> Remember me 
                    <a href="/qoute">
                    <button type="button" className="cancelbtn"><Link to="/" >Cancel</Link></button> 
                    </a>
                    Forgot <a href="/"> password? </a>  
                </LoginForm>
            </div>
        </>
    );

};

export default Login;