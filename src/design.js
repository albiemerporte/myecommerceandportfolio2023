import styled, { createGlobalStyle } from 'styled-components';
import { Link } from 'react-router-dom';

// <Globe /> i App.js
export const Globe = createGlobalStyle`

    @import url(//fonts.googleapis.com/css?family=Open+Sans:400,700,800,300);

`;

// <Body> in App.js
export const Body = styled.body`
        /*background-color: #06fc1d; */
        margin: 0;
	    padding: 0;
	    font-family: 'Open Sans', 'Helvetica Neue', Helvetica,sans-serif;
	    line-height: 1.45;

`;

export const StyledLink = styled(Link)`

    text-decoration: none;
    color: #ff3846;
    float: right;

    &:hover {
        
        color: #1291e9;
        display:block;
        

    }

    @media screen and (max-width: 480px){
        
        text-decoration: none;
        color: #333;
        

        &:hover {
            color: #e50c6f;
            transition-delay: 0.3s;
            display:block;
        }
    }

`;

export const StyledLogin = styled(Link)`

    width: 10%;
    height: auto;
    padding: 5px;
    border: 3px solid #101e12;
    border-radius: 20px;
    color:#ffffff;
    font-size: 20px;
    text-decoration: none;
    margin: 20px 10px 0 0;
    

    &:hover {
        color: #569c30;
    }

    @media screen and (max-width: 480px) {
        margin: 0 0 0 0;
        width: 100%;
    }

`;

// <FeaturesSection> in features.js
export const FeaturesSection = styled.section`

    padding: 80px 0;
    text-align: center;
    background: #ffffff;
    color: #000000;

    .Container {
        margin: 0 auto;
        padding: 0 20px 0 20px;
        max-width: 900px;
    }

`;

// <FeatureDiv> in features.js
export const FeatureDiv = styled.div`

    width: 32%;
    display: inline-block;
    fonr-size: 16px;

    @media screen and (max-width: 480px) {

        width: 100%;
        text-align: left;
        margin: 0 0 10px 0;
        font-size: 16px;
        display: flex;
        align-items: center;
        justify-content: center;

    }

`;

export const CompanyLogo = styled.img`

    width: 40%;

    @media screen and (max-width: 480px) {
        width:15%;
        min-width:60px;
        margin-right:20px;
    }

`;

// <FeatureDivImg> in fetures.js
export const FeatureDivImg = styled.img`

    width: 40%; 
    
    @media screen and (max-width: 480px) {
        width:15%;
        min-width:60px;
        margin-right:20px;
    }   

`;

// <QouteSection> in qoute.js
export const QouteSection = styled.section.attrs(props => ({

    id: props.id || 'qoute',

}))`

    padding: 40px 0;
    text-align: center;
    background: #549DA0;
    color: #FFFFFF;
    font-size: 30px;

    .Container {
        margin: 0 auto;
        padding: 0 20px 0 20px;
        max-width: 900px;

        p {
            margin: 0 0 5px 0;
            font-size: 24px;
        }
    
        cite {
            content:'-';
            margin-right: 5px;
        
    
            &:after {
                font-size: 16px;
                font-style: italic;
            }
        }

    }

`;
