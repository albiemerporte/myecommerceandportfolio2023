
import styled from 'styled-components';

// <FooterSection> in footer.js
export const FooterSection = styled.footer.attrs(props => ({

    id: props.id || 'footer',

}))`

    background: #353535;
    padding: 30px 0 10px 0;
    text-align: center;
    color: #868686;

    .container {
        
        margin: 0 auto;
        padding: 0 20px 0 20px;
        max-width: 900px;

    }

    p { text-align: center; }

    a {
        padding: 6px;
        font-size:14px;
        text-decoration: none;
        color: #c3c3c3;
        

        &:hover {
            color: #a3719f;
        }

    }

`;