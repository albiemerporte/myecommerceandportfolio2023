import styled from 'styled-components';

// <HeaderSection> in header.js
export const HeaderSection = styled.header.attrs(props => ({

    id: props.id || 'header',

}))`

    .headerContainer { 
        display: grid;
        grid-template-columns: 40% 55%;
        grid-gap: 5%;

        margin: 0 auto;
        padding: 0 20px 0 20px;
        max-width: 900px;
        min-height:  80px;

    }

    .lefthead { border: solid 1px red;}

    .righthead { border: solid 1px green; padding: 5%;}

    color: #FFFFFF;
    background-color: #8e8e8e; /*#ffffff; #9cf5f8; */
    padding: 5% 2% 2% 2%;
    text-align: center;
    Border: 0 0 1px 0 solid #fa0a0a;

    .menu {
        /*background: #d0a900;*/
        top:0;
        left: 0;
        position: absolute;
        left: -25px;
        transition: 0.3s;
        padding: 20px;
        width: 60px;
        text-decoration: none;
        font-size: 20px;
        color: white;
        border-radius: 0 50px 50px 0;
        position: fixed;
        transition-delay: 0.1;
        

    }

    .menuclick { display: block; margin: 0 0 0 20px; float: right; }

    .menu:hover{

        flex-direction: column;
        background:#5c5f60; /* #9df5f8; bg of menu*/ 
        width: 500px;
        text-align: left;
        display: block;
        color: #ffffff;
        border: 3px solid #353535; /*#1291e9;*/

        .menu-list {
            margin: 0 0 0 10px;
            border-top: 1px solid #ffffff; /* whoiteshit */
            padding: 10px 0 0 0;
        } 

        .menuclick {
            display: none;
        }

        li {
            display: block;
        }

    }

    ul {
        
        list-style: none;
        margin: 0;
        padding: 0;
        display: flex;
        justify-content: space-between;
    }

    li {
        margin: 0 10px;
        display: none;
    }

    @media screen and (max-width: 480px){

        .Conatiner { display: inline-block; }

        padding: 25px 0 15px 0;
        border: solid 1px red;

        .menuclick {
            
            display: block;
            margin: 0 0 0 15px;
        }


        .menu:hover {
            width: 90px;

            li {
                text-align: left;
                margin: 30px 0 0 0;
                display: block;
            }

            .about {
                margin-top: 30px;
            }

        }

        ul {
            flex-direction: column;
        }

        li {
            display: none;
        }
    
    }

    

`;