
import styled from 'styled-components';

export const LoginForm = styled.form`

    /*border: 3px solid #f1f1f1;*/
    width: 100%;
    

    .loginbutton {
        background-color: #4CAF50; 
        width: 100%;
        height: auto;
        color: orange; 
        padding: 15px; 
        margin: 20px 0px; 
        border: solid 1px single; 
        cursor: pointer;

        &:hover {
            opacity: 0.7;
        }
    }

    input[type=text], input[type=password] { 
        width: 100%; 
        margin: 20px 0;
        padding: 12px 20px; 
        display: inline-block; 
        border: 2px solid #aa0000; 
        box-sizing: border-box; 
    }

    .cancelbtn {
        text-decoration: none;
        width: auto; 
        padding: 10px 18px;
        margin: 10px 5px;
    }

    .container { 
        padding: 25px; 
        background-color: #9cf5f8;
    } 

`;