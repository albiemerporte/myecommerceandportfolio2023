import styled from "styled-components";


// <FeaturesSection> in features.js
export const VlogSection = styled.section`

    padding: 80px 0;
    text-align: center;
    background: #549da0;
    color: #000000;

`;

// <FeatureDiv> in features.js
export const VlogFeatDiv = styled.div`

    width: 32%;
    display: inline-block;
    fonr-size: 16px;
    margin: 0 10px 20px 10px;

    @media screen and (max-width: 480px) {

        width: 100%;
        text-align: left;
        margin: 0 0 10px 0;
        font-size: 16px;
        display: flex;
        align-items: center;
        justify-content: center;

    }

`;


// <FeatureDivImg> in fetures.js
export const VlogDivImg = styled.img`

    width: 100%;
    height: auto;
    margin: 0 10px 30px 10px;
    border: solid 5px #048de2;

    @media screen and (max-width: 480px) {
        width:100%;
        min-width:60px;
        margin:0 0 20px 0;
    }   

`;