


import { FooterSection } from './designcss/footercss'

const Footer = () => {

    return (
        <FooterSection>
            <div className = "container">
                    <a>Home</a>
                    <a>About</a>
                    <a>Contacts</a>
                <p>&copy; All rights reserved</p>
            </div>
        </FooterSection>
    );

};

export default Footer;