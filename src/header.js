import { HeaderSection }  from './designcss/headercss';
import { StyledLink, StyledLogin } from './design';
import menu from './images/menu.svg';
import styled from 'styled-components';
import headimage from './images/90.svg';

const HeaderImg = styled.img`

    width: 100%;
    height: auto;

`;

const Header = () => {
    return(
        <HeaderSection>
            <div className='headerContainer'>
                <nav class="menu">
                    <ul class="menu-list">
                        <li className="menuclick">
                            <img src={ menu } alt="menu-icon" style={{width:'60px', height:'60px'}}/></li>
                        <li><StyledLink to=''></StyledLink></li>
                        <li><StyledLink to='/'>Home</StyledLink></li>
                        <li><StyledLink to='/features'>About</StyledLink></li>
                        <li><StyledLink to='/project'>Project</StyledLink></li>
                        <li><StyledLink to='/vlogs'>Vlogs</StyledLink></li>
                        <li><StyledLink to='/features'>Features</StyledLink></li>
                        <li><StyledLink to='/qoute'>Qoute</StyledLink></li>
                    </ul>
                </nav>
                <div className='lefthead'>
                    <HeaderImg src={ headimage } />
                </div>
                <div className='righthead'>
                    <StyledLogin to="/login">ALBIEMER PORTE</StyledLogin>
                    <p>Objective is to get hired in office and to fuck beatiful department office</p>
                </div>
            </div>
        </HeaderSection>
    );
};

export default Header;