
import { ProjFeatDiv, ProjDivImg, ProjSection } from "./designcss/projectcss";
import { HeaderSection } from "./designcss/headercss";
//image import
import proj1 from './images/projectimg/accessfolderfrom.jpg'
import proj2 from './images/projectimg/accesswindowsshared.webp'
import proj3 from './images/projectimg/createvirt.webp'
import proj4 from './images/projectimg/dhcp.webp'

const Project = () => {

    return (   
        <>  
            <ProjSection>        
                <ProjFeatDiv>
                    <ProjDivImg alt='poject' src={ proj1 } />
                    <p>This is features</p>
                </ProjFeatDiv>

                <ProjFeatDiv>
                    <ProjDivImg alt='poject' src={ proj2 } />
                    <p>This is features</p>
                </ProjFeatDiv>
                    
                <ProjFeatDiv>
                    <ProjDivImg alt='poject' src={ proj3 } />
                    <p>This is features</p>
                </ProjFeatDiv>

                <ProjFeatDiv>
                    <ProjDivImg alt='poject' src={ proj4 } />
                    <p>This is features</p>
                </ProjFeatDiv>
            </ProjSection>
        </>

    );

};

export default Project;