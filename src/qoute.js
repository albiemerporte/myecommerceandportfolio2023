
import { QouteSection } from "./design";

const Qoute = () => {

    return (
        <QouteSection>
            <div className="Container">
                <p>"Some Great Qoute"</p>
                <cite>Satisfied Customer</cite>
            </div>
        </QouteSection>
    );

};

export default Qoute;