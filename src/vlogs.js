
import { VlogFeatDiv, VlogDivImg, VlogSection } from './designcss/vlogscss'

//image import
import proj1 from './images/projectimg/accessfolderfrom.jpg'
import proj2 from './images/projectimg/accesswindowsshared.webp'
import proj3 from './images/projectimg/createvirt.webp'
import proj4 from './images/projectimg/dhcp.webp'
import proj5 from './images/projectimg/howtomakeintro.jpg'

const Vlogs = () => {

    return (    
            <VlogSection>
                <div className='Container'>
                    <div className='vlogdivleft'>
                        <p>This is div left</p>    
                    </div>
                    <div className='vlogdivright'>          
                        <VlogFeatDiv>
                            <VlogDivImg alt='poject' src={ proj1 } />
                            <p>This is features</p>
                        </VlogFeatDiv>

                        <VlogFeatDiv>
                            <VlogDivImg alt='poject' src={ proj2 } />
                            <p>This is features</p>
                        </VlogFeatDiv>
                            
                        <VlogFeatDiv>
                            <VlogDivImg alt='poject' src={ proj3 } />
                            <p>This is features</p>
                        </VlogFeatDiv>

                        <VlogFeatDiv>
                            <VlogDivImg alt='poject' src={ proj4 } />
                            <p>This is features</p>
                        </VlogFeatDiv>

                        <VlogFeatDiv>
                            <VlogDivImg alt='poject' src={ proj5 } />
                            <p>This is features</p>
                        </VlogFeatDiv>
                    </div>
                </div>
            </VlogSection>
    );

};

export default Vlogs;